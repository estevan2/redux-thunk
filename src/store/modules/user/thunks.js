import { addComment } from "./actions";

export const addCommentThunk = (comment) => {
    return (dispatch, getState) => {
        const { user } = getState()
        const updatedUser = { name: user.name, comments: [...user.comments, comment] }
        dispatch(addComment(updatedUser))
    }
}