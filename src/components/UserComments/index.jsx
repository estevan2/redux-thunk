import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCommentThunk } from "../../store/modules/user/thunks";
import logo from "../../assets/logoblue.svg"

const UserComments = () => {

    const [newComment, setNewComment] = useState()
    const dispatch = useDispatch()
    const user = useSelector(state => state.user)

    return (
        <div className="container">
            <div className="container__name_user">
                <div className="image_container">
                    <img src={logo} alt="avatar" />
                </div>
                <h1>{user.name}</h1>
            </div>
            <div className="comments_container">
                <div>
                    {user.comments.map(comment => <p className="comment">{comment}</p>)}
                </div>
                <input placeholder="Comment..." onChange={e => setNewComment(e.target.value)} />
                <button onClick={() => dispatch(addCommentThunk(newComment))}>new comment</button>
            </div>
        </div>
    )
}

export default UserComments